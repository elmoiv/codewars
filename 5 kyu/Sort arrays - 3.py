def sort_key(course):
    name, id = course.split('-')
    yy, mm = int(id[:2]), int(id[2:])
    return yy, mm, name

def sort_me(courses): 
    return sorted(courses, key=sort_key)

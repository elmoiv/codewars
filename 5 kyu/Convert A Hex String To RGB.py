def hex_string_to_RGB(h):
    return {r:int(''.join(x), 16) for r, x in zip('rgb', zip(h[1::2], h[2::2]))}

def beeramid(b, p, l=[1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169]):
    return beeramid(b, p, l[:-1]) if b // p < sum(l) and l else len(l)

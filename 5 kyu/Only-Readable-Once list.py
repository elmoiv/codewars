class SecureList(list):
    def __init__(self, l):
        self.l = l[:]
    
    def __getitem__(self, i):
        return self.l.pop(i)
    
    def __repr__(self):
        a, self.l = self.l[:], []
        return str(a)
    
    def __str__(self):
        return self.__repr__()
    
    def __len__(self):
        return len(eval(self.__repr__()))

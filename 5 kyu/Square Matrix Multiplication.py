from numpy import array
def matrix_mult(a, b):
    return (array(a) @ array(b)).tolist()

Position = {'high': 'h', 'low': 'l'}

class Warrior():
    def __init__(self, name):

        self.name = name
        self.health = 100

        self.block = ""
        self.deceased = False
        self.zombie = False
        
    def attack(self, enemy, position):
        damage = 0

        if enemy.block != position:
            damage += 10 if position == 'h' else 5

        if enemy.block == "":
            damage += 5

        enemy.set_health(enemy.health - damage)
    
    def set_health(self, new_health):
        self.health = max(0, new_health)
        
        if self.deceased:
            self.zombie = True
        
        if self.health == 0:
            self.deceased = True
            self.zombie = False
